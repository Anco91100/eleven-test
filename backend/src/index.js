const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const astronautsRouter = require('./routes/astronaut');
const pg = require('pg');
const Pool = pg.Pool
app.use(bodyParser.json());
app.use(cors({
    origin: "*"
}));
const pool = new Pool({
    host: 'database',
    port: 5432,
    database: 'astronauts',
    user: 'postgres',
    password: 'postgres'
});

// pool.query('CREATE TABLE astronauts ( id serial PRIMARY KEY, name varchar(255) NOT NULL, agency varchar(255) NOT NULL, date_of_birth date, date_of_death date );');

const astronautsTableExists = async () => {
    await pool.connect();
    const result = await pool.query(`SELECT 1 FROM pg_catalog.pg_tables WHERE tablename = 'astronauts'`);
    return result.rows[0].count;
};

if (!astronautsTableExists()) {
    pool.query('CREATE TABLE astronauts ( id serial PRIMARY KEY, name varchar(255) NOT NULL, agency varchar(255) NOT NULL, date_of_birth date, date_of_death date );');
}

// app.use('/astronauts', astronautsRouter);


app.get('/astronauts', async (req, res) => {
    const astronauts = await pool.query('SELECT * FROM astronauts');

    res.json(astronauts);
});

app.post('/astronauts', async (req, res) => {
    const astronautData = req.body;

    await pool.query('INSERT INTO astronauts (name, agency, date_of_birth, date_of_death) VALUES ($1, $2, $3, $4)', [astronautData.name, astronautData.agency, astronautData.dateOfBirth, astronautData.dateOfDeath]);

    res.json(astronautData);
});

app.put('/astronauts/:id', async (req, res) => {
    const astronautId = req.params.id;

    const astronautData = req.body;

    await pool.query('UPDATE astronauts SET name = $1, agency = $2, date_of_birth = $3, date_of_death = $4 WHERE id = $5', [astronautData.name, astronautData.agency, astronautData.date_of_birth, astronautData.date_of_death, astronautId]);

    res.json(astronautData);
});

app.delete('/astronauts/:id', async (req, res) => {
    const astronautId = req.params.id;

    await pool.query('DELETE FROM astronauts WHERE id = $1', [astronautId]);

    res.status(200).send('');
});

app.listen(4000, () => {
    console.log('Server started on port 3000');
});