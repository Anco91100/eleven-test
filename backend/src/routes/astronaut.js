const Router = require('express').Router
const AstronautsController = require("../controllers/astronauts");
const AstronautsModel =  require("../model/astronaut");
const pg = require('pg');
const Pool = pg.Pool
const pool = new Pool({
    host: 'database',
    port: 5432,
    database: 'astronauts',
    user: 'postgres',
    password: 'postgres'
});

const router = Router();
const astronautsController = new AstronautsController(pool)
router.get("/", astronautsController.getAstronauts);
router.post("/", astronautsController.addAstronaut);
router.put("/:id", astronautsController.updateAstronaut);
router.delete("/:id", astronautsController.deleteAstronaut);

module.exports = router;