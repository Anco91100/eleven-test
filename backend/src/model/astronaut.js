class AstronautsModel {
    constructor(connection,table) {
        this.connection = connection;
        this.table = table;
    }
  
    async getAstronauts() {
      const query = `SELECT * FROM ${this.table}`;
      const astronauts = await this.connection.query(query);
      return astronauts.rows;
    }
  
    async addAstronaut(astronaut) {
      const query = `INSERT INTO ${this.table} (name, agency, date_of_birth, date_of_death) VALUES ($1, $2, $3, $4)`;
      const values = [astronaut.name, astronaut.agency, astronaut.dateOfBirth, astronaut.dateOfDeath];
      await this.connection.query(query, values);
    }
  
    async updateAstronaut(astronaut) {
      const query = `UPDATE ${this.table} SET name = $1, agency = $2, date_of_birth = $3, date_of_death = $4 WHERE id = $5`;
      const values = [astronaut.name, astronaut.agency, astronaut.dateOfBirth, astronaut.dateOfDeath, astronaut.id];
      await this.connection.query(query, values);
    }
  
    async deleteAstronaut(id) {
      const query = `DELETE FROM ${this.table} WHERE id = $1`;
      const values = [id];
      await this.connection.query(query, values);
    }
  }

  module.exports = AstronautsModel;