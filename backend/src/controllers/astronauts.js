const AstronautsModel = require("../model/astronaut");

class AstronautsController {
    constructor(pool) {
        this.astronauts = new AstronautsModel(pool, 'astronauts');
      }
    getAstronauts() {
      return this.astronauts.getAstronauts();
    }
  
    addAstronaut(astronaut) {
      this.astronauts.addAstronaut(astronaut);
    }
  
    updateAstronaut(astronaut) {
      this.astronauts.updateAstronaut(astronaut);
    }
  
    deleteAstronaut(id) {
      this.astronauts.deleteAstronaut(id);
    }
  }

  module.exports = AstronautsController;
  