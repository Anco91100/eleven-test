import {  Route, Routes } from "react-router-dom";
import AstronautEdit from "./AstronautEdit";
import AstronautsListContainer from "./AstronautsListContainer";
export default function App() {
    return (
          <Routes>
            <Route path="/" element={<AstronautsListContainer />} />
            <Route path="/edit/:astronautId" element={<AstronautEdit />} />
          </Routes>
    );
  }