import React, { useState, useEffect } from "react";
import { getAstronauts, deleteAstronaut } from "./api";
import AstronautsList from "./AstronautsList";
import AstronautForm from "./AstronautForm";

const AstronautsListContainer = () => {
    const [astronauts, setAstronauts] = useState([]);

    useEffect(() => {
        getAstronauts().then((response) => {
            setAstronauts(response.rows);
        });
    }, []);

    return (
        <div>
            <AstronautForm />
            <AstronautsList astronauts={astronauts} deleteAstronaut={deleteAstronaut} />
        </div>
    );
};

export default AstronautsListContainer;