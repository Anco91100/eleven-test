import axios from "axios";

const astronautsAPI = axios.create({
  baseURL: "http://localhost:4000/",
});

export default astronautsAPI;

export async function getAstronauts() {
  const response = await astronautsAPI.get("/astronauts");
  return response.data;
}

export async function getAstronautById(id) {
  const response = await astronautsAPI.get(`/astronauts/${id}`);
  return response.data;
}

export async function createAstronaut(astronaut) {
  const response = await astronautsAPI.post("/astronauts", astronaut);
  return response.data;
}

export async function updateAstronaut(id, astronaut) {
  const response = await astronautsAPI.put(`/astronauts/${id}`, astronaut);
  return response.data;
}

export async function deleteAstronaut(id) {
  const response = await astronautsAPI.delete(`/astronauts/${id}`);
  return response.data;
}
