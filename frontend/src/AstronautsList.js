import React from "react";
import { Link } from "react-router-dom";

const AstronautsList = ({astronauts, deleteAstronaut}) => {

  const onDeleteAstronaut = async (id) => {
    await deleteAstronaut(id)
  };


  return (
    <div>
      <ul>
        {astronauts.map((astronaut) => (
          <div>
            <li className="flex flex-row items-center justify-center gap-4 p-4" key={astronaut.id}>
              <p>{astronaut.name}</p>
              <p>{astronaut.agency}</p>
              <p>{astronaut.date_of_birth}</p>
              <p>{astronaut.date_of_death}</p>
              <Link className='flex flex-row items-center justify-center p-4 gap-2 w-20 h-7 rounded-sm  border-solid border-[#e98b10] bg-[#e98b10] text-white text-center text-sm font-normal' to={`/edit/${astronaut.id}`} state={{ astronaut }} > Edit </Link>
              <button onClick={() => onDeleteAstronaut(astronaut.id)} className='flex flex-row items-center justify-center p-4 gap-2 w-20 h-7 rounded-sm  border-solid border-[#104EE9] bg-[#104EE9] text-white text-center text-sm font-normal'> Delete </button>
            </li>
          </div>
        ))}
      </ul>
    </div>
  );
};

export default AstronautsList;
