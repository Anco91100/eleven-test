import React, { useState } from "react";
import { Formik, Field, ErrorMessage } from 'formik';
import { createAstronaut } from "./api";

const AstronautForm = () => {
    const [astronaut, setAstronaut] = useState({
      name: "",
      agency: "",
      dateOfBirth: "",
      dateOfDeath: "",
    });
  
    const handleSubmit = (values) => {
      createAstronaut(values).then(() => {
        setAstronaut({
          name: "",
          agency: "",
          dateOfBirth: "",
          dateOfDeath: "",
        });
      });
    };
  
    return (
      <Formik initialValues={astronaut} onSubmit={handleSubmit}>
        {({ handleSubmit, errors }) => (
          <form className='w-full h-full' onSubmit={handleSubmit}>
            <div className='flex flex-col w-full h-[500px] items-start p-4 gap-4 bg-white shadow-[0px_4px_20px_rgba(0,0,0,0.08)]' >
              <div className='flex flex-col min-w-full items-start h-16 gap-2'>
                <h2> Astronaut info</h2>
                <div className='border border-solid border-[rgba(188,202,220,0.5)] w-full'></div>
              </div>
              <Field
                className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center"
                name="name"
                label="Name"
                type="text"
                placeholder="First Name"
                required
              />
              <ErrorMessage name="name" component="div" />
  
              <Field
                className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center"
                name="agency"
                label="Agency"
                type="text"
                placeholder="Agency"
                required
              />
              <ErrorMessage name="agency" component="div" />
  
              <Field
                className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center"
                name="dateOfBirth"
                label="Date of Birth"
                type="date"
                placeholder="Date of Birth"
                required
              />
              <ErrorMessage name="dateOfBirth" component="div" />
  
              <Field
                className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center"
                name="dateOfDeath"
                label="Date of Death"
                type="date"
                placeholder="Date of Death"
              />
              <ErrorMessage name="dateOfDeath" component="div" />
            </div>
            <div className='flex flex-row justify-end items-start p-4 h-7 w-full'>
              <button type="submit" className='flex flex-row items-center justify-center p-4 gap-2 w-20 h-7 rounded-sm  border-solid border-[#104EE9] bg-[#104EE9] text-white text-center text-sm font-normal'> Save </button>
            </div>
          </form>
        )}
      </Formik>
    );
  };

export default AstronautForm;