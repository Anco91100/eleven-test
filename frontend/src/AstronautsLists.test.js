import React from "react";
import { render, screen } from "@testing-library/react";
import AstronautsList from "./AstronautsList";

describe("AstronautsList", () => {
    it("should render a list of astronauts", () => {
        const astronauts = [
            {
                id: "1",
                name: "Neil Armstrong",
                agency: "NASA",
                date_of_birth: "1930-08-05",
                date_of_death: "2012-08-25",
            },
        ];
        const wrapper = render(<AstronautsList astronauts={astronauts} deleteAstronaut={()=>{}} />);
        expect(screen.queryAllByText("Neil Armstrong")).toHaveLength(1);
    });
});